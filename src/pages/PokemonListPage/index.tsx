import { LinearProgress, Pagination } from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import useSWR from "swr";
import PokemonCardsList, {
  TPokemonList,
} from "../../components/PokemonCardsList";
import { BASE_API_URL } from "../../constants/urls";
import fetch from "../../libs/fetch";
import { IApiResponsePokemonList } from "../../types/ApiResponse";

const PokemonListPage: React.FC = () => {
  const [page, setPage] = useState<number>(1);
  const limit = 12;
  const offset = (page - 1) * limit;
  const url: string = BASE_API_URL + `pokemon?limit=${limit}&offset=${offset}`;
  const { data } = useSWR<IApiResponsePokemonList>(url, fetch);
  const totalCount: number = data?.count ? Math.ceil(data.count / limit) : 1;
  const pokemonList: TPokemonList = data?.results || Array(limit).fill(null);

  return (
    <>
      <Box sx={{ width: "100%", height: "20px" }}>
        {!data && <LinearProgress />}
      </Box>
      <PokemonCardsList pokemonList={pokemonList} />
      <Box
        display="flex"
        width={"100%"}
        alignItems="center"
        justifyContent="center"
        mt={2}
      >
        <Pagination
          count={totalCount}
          onChange={(e, page) => setPage(page)}
          page={page}
        />
      </Box>
    </>
  );
};

export default PokemonListPage;
