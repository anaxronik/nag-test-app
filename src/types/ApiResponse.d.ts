export interface IApiResponsePokemonList {
  count: number;
  next: string;
  previous: string;
  results: TPokemonListItem[];
}

export type TPokemonListItem = {
  name: string;
  url: string;
};

/** Интерфейс описан не полностью */
export interface IApiResponsePokemonItem {
  abilities: {
    is_hidden: boolean;
    slot: number;
    ability: {
      name: string;
      url: string;
    };
  }[];
  base_experience: number;
  forms: { name: string; url: string }[];
  game_indices: { game_index: number; version: { name: string } }[];
  height: number;
  held_items: any[];
  id: number;
  is_default: boolean;
  location_area_encounters: string;
  name: string;
  url: string;
  sprites: {
    front_default: string | null;
  };
  weight: number;
}
