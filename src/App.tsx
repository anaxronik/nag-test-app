import Container from "@mui/material/Container";
import React from "react";
import PokemonListPage from "./pages/PokemonListPage";

const App: React.FC = () => {
  return (
    <div className="app">
      <Container maxWidth="xl">
        <PokemonListPage />
      </Container>
    </div>
  );
};

export default App;
