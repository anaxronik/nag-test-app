import { Avatar, Card, CardContent, Skeleton, Typography } from "@mui/material";
import React from "react";

interface IProps {
  name?: string;
  avatarUrl?: string;
  height?: number;
  weight?: number;
}

const PokemonCard: React.FC<IProps> = ({ name, avatarUrl, height, weight }) => {
  return (
    <Card>
      <CardContent>
        <>
          {avatarUrl ? (
            <Avatar
              alt={name || undefined}
              src={avatarUrl || undefined}
              sx={{ width: 64, height: 64 }}
            />
          ) : (
            <Skeleton variant="circular" width={64} height={64} />
          )}
          <Typography variant="h5" component="div">
            {name ? name : <Skeleton variant="text" />}
          </Typography>
          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            {height ? `Рост: ${height} ед.` : <Skeleton variant="text" />}
          </Typography>
          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            {weight ? `Рост: ${weight} ед.` : <Skeleton variant="text" />}
          </Typography>
        </>
      </CardContent>
    </Card>
  );
};

export default PokemonCard;
