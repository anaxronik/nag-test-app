import { Grid } from "@mui/material";
import React from "react";
import { TPokemonListItem } from "../../types/ApiResponse";
import PokemonCard from "../PokemonCard";
import PokemonCardsListItem from "../PokemonCardsListItem";

export type TPokemonList = (TPokemonListItem | null)[];

interface IProps {
  pokemonList: TPokemonList;
}

const PokemonCardsList: React.FC<IProps> = ({ pokemonList }) => {
  return (
    <Grid container spacing={2}>
      {pokemonList.map((pokemon, idx) => (
        <Grid item xl={2} lg={3} md={4} xs={12} key={idx}>
          {pokemon?.name ? (
            <PokemonCardsListItem name={pokemon.name} />
          ) : (
            <PokemonCard />
          )}
        </Grid>
      ))}
    </Grid>
  );
};

export default PokemonCardsList;
