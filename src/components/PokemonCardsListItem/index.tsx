import React from "react";
import useSWR from "swr";
import { BASE_API_URL } from "../../constants/urls";
import { IApiResponsePokemonItem } from "../../types/ApiResponse";
import PokemonCard from "../PokemonCard";
import fetch from "../../libs/fetch";

interface IProps {
  name: string;
}

const PokemonCardsListItem: React.FC<IProps> = ({ name }) => {
  const url: string = BASE_API_URL + `pokemon/${name}`;
  const { data } = useSWR<IApiResponsePokemonItem>(url, fetch);

  return (
    <PokemonCard
      avatarUrl={data?.sprites.front_default || undefined}
      height={data?.height}
      name={data?.name}
      weight={data?.weight}
    />
  );
};

export default PokemonCardsListItem;
